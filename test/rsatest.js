/**
 * Created by peipeidediannao on 16/8/30.
 */
'use strict';
const NodeRSA = require('node-rsa');
const urlEncode = require('urlencode');
const _ = require('lodash');

const keystr = {
    ownerPublicDer: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDib/c3amsgkDB9zNGXnVqtLg5T1AzNcunDBaAElaYLHXVjQ46dSf8nfsF9kretyAmI3a1qRcmwRKlRFMqJuoM+e3aIDisOFwtTEqci1vSnQY8+62+8QZXtMNlAmvMFy5tbfdhuAPPFmxR91dFCBTygF5o8V7ddCM6oVzNa7bWZ9wIDAQAB',
    ownerPrivateDer: 'MIICXAIBAAKBgQDib/c3amsgkDB9zNGXnVqtLg5T1AzNcunDBaAElaYLHXVjQ46dSf8nfsF9kretyAmI3a1qRcmwRKlRFMqJuoM+e3aIDisOFwtTEqci1vSnQY8+62+8QZXtMNlAmvMFy5tbfdhuAPPFmxR91dFCBTygF5o8V7ddCM6oVzNa7bWZ9wIDAQABAoGAD2AO50Jgl8tyn3gi2yqiF5RDAMorPGALI2TkYqtOpP6RE/ZoXqHBo4rl1IL72d71VsfGmKq4cTGYVZsVzZhOhPTgpWtvytQFxPwAZO4x99Q8Ke0uWWRIor5a0GRFiZMRMegtD8tnA86yILYJat9JkLVQgKYdfGda24+hIL4kDfECQQD6MPdKRlf96LHaIu9SvNAWRVcvhPYvmt62BXK33kPTB0hVrzE4ELASN7vCB2ZUKUZa316pQr2EiQf4OaTJcZvDAkEA57HQ9jfhtxKxr92L2ICrb6Hp9TL+/JQjIgeJXbwsXPprNgbzz/lV3h9o5jgHmkTwo+xXmwDYxCuvpTwxSzlJvQJAY6B2CKSkS16K2/NHW6oGLqgkBK8snV5NSyw2N1ORrInc2wntTocOpeJ9qNDrhZ39/xetiTcjYpBylPzXSsQrowJBALwqBPJxX7Y2UGfrHO+vT4tRi5e+2qZ2CDJSvJSLhdvc48T00H1OmRXTnHWbe1EmO+hkwKD5hxGKCRPAVqgo1mECQGly3vnF24LbXZyG4Ta+b0/MWpEVkDgIMgweKaFsJIS+NftVt1UI4G/3UR8Pv0OdZFMw1pxBp4lZhLy7roI7m0Y=',
    zmxyPublicDer:'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCL2M4jZitbwhWqvCtWmh5XFBMgSbSVz45KK0y5BnV7812ko/dEsipBFrclwGyrKUHq+6jn+aJ3a6gJND5q5+04mIeQYgG1vCDmMReE+ZvKoAztK0bu/5oxBMTco8hwVEFny5Z6q9P5pEh+2JZ9aJmRdIhLfQEFHUNBEiPGUnoDhQIDAQAB'
}

const owner_private_key = new NodeRSA(keystr.ownerPrivateDer, 'private');
const zhima_public_key = new NodeRSA(keystr.zmxyPublicDer, 'public');



const merchantId = `268820000049436291974`;
const authType = '0';
const authParam = {"certNo":"333333199001011234","certType":"IDENTITY_CARD","name":"张三"};
let dataArray = {
    merchantId,
    authType,
    authParam
}
let _data = []
Object.keys(dataArray).map(item => {
    let val = item === 'authParam' ? JSON.stringify(dataArray[item]): dataArray[item];
    _data.push(`${item}=${urlEncode.encode(val)}`)
})


// 明文数据
let dataPlain = _data.join('&')

// zhima pulbic croyto

let params = urlEncode.encode(zhima_public_key.encrypt(dataPlain).toString('base64'))
let sign = urlEncode.encode(owner_private_key.encrypt(dataPlain).toString('base64'))

console.log(params)
console.log(sign)

