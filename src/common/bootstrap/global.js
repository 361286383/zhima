/**
 * this file will be loaded before server started
 * you can define global functions used in controllers, models, templates
 */

/**
 * use global.xxx to define global functions
 *
 * global.fn1 = function(){
 *     
 * }
 */
import crypto from 'crypto';
import xml2js from 'xml2js';
import urlencode from 'urlencode';
/*
 将对象属性进行拼接
 */
global.raw = function (args) {
    let keys = Object.keys(args);
    keys = keys.sort()
    let newArgs = {};
    keys.forEach(function (key) {
        newArgs[key.toLowerCase()] = args[key];
    });

    let string = '';
    for (let k in newArgs) {
        string += '&' + k + '=' + newArgs[k];
    }
    string = string.substr(1);
    return string;
};

/*
 将对象属性值进行拼接
 */
global.rawvalue = function (args) {
    Array.sort(args);
    return args.join('');
};

/*
 加载微信post的xml数据
 */
global.loadxml = function (stream, callback) {
    if (stream.rawBody) {
        callback(null, stream.rawBody);
        return;
    }

    var buffers = [];
    stream.on('data', function (trunk) {
        buffers.push(trunk);
    });
    stream.on('end', function () {
        callback(null, Buffer.concat(buffers));
    });
    stream.once('error', callback);
}
/**
 *
 * @param stream
 * @returns {Promise}
 */
global.loadxmlAsync = function (stream) {
    return new Promise(function (resolve, reject) {
        global.loadxml(stream, function (err, buf) {
            if (err) {
                reject('load xml failed')
            } else {
                resolve(buf)
            }
        })
    })
}
/**
 * 
 * @param xml
 * @returns {Promise}
 */
global.parseXmlStringAsync = function (xml) {
    return new Promise((resolve,reject) => {
        xml2js.parseString(xml, {trim: true}, function (err, result) {
            if (err) {
                reject('parse xml failed')
            } else {
                resolve(result)
            }
        })
    })
}
/*
 xml数据格式化为JSON
 */
global.formatXml2Json = function (result) {
    var message = {};
    if (typeof result === 'object') {
        for (var key in result) {
            if (!(result[key] instanceof Array) || result[key].length === 0) {
                continue;
            }
            if (result[key].length === 1) {
                var val = result[key][0];
                if (typeof val === 'object') {
                    message[key] = formatMessage(val);
                } else {
                    message[key] = (val || '').trim();
                }
            } else {
                message[key] = [];
                result[key].forEach(function (item) {
                    message[key].push(formatMessage(item));
                });
            }
        }
    }
    return message;
}

/**
 * AES加密(base64格式)
 */
global.aesEncryption = function (data, key) {
    var iv = "";
    var clearEncoding = 'utf8';
    var cipherEncoding = 'base64';
    var cipherChunks = [];
    var cipher = crypto.createCipheriv('aes-128-ecb', key, iv);
    cipher.setAutoPadding(true);

    cipherChunks.push(cipher.update(data, clearEncoding, cipherEncoding));
    cipherChunks.push(cipher.final(cipherEncoding));

    return cipherChunks.join('');
}

/*
 AES解密
 */
global.aesDecryption = function (data, key) {
    var iv = "";
    var clearEncoding = 'utf8';
    var cipherEncoding = 'base64';
    var cipherChunks = [];
    var decipher = crypto.createDecipheriv('aes-128-ecb', key, iv);
    decipher.setAutoPadding(true);

    cipherChunks.push(decipher.update(data, cipherEncoding, clearEncoding));
    cipherChunks.push(decipher.final(clearEncoding));

    return cipherChunks.join('');
}
/**
 * [format 日期格式化]
 * @param  {[Int]} num [整数]
 * @param  {[String]} str [字符串]
 * @return {[String]}     [字符串]
 * <pre>
 * tools.dateFormat(1392780164805);   结果：2014-02-19 11:22:44
 * tools.dateFormat(1392780164805,"YYYY-MM-dd");   结果：2014-02-19
 * </pre>
 */
global.dateFormat = function (str) {
    var d = new Date();
    str = str || "YYYY-MM-dd hh:mm:ss";
    var o = {
        "M+": d.getMonth() + 1, // month
        "d+": d.getDate(), // day
        "h+": d.getHours(), // hour
        "m+": d.getMinutes(), // minute
        "s+": d.getSeconds(), // second
        "q+": Math.floor((d.getMonth() + 3) / 3), // quarter
        "S": d.getMilliseconds()   // millisecond
    };
    if (/(y+)/ig.test(str)) {
        str = str.replace(RegExp.$1, (d.getFullYear() + "")
            .substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(str)) {
            str = str.replace(RegExp.$1, RegExp.$1.length === 1 ? o[k]
                : ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return str;
};
/**
 * 对URL参数进行编码
 * @param str
 * @returns {*}
 * @constructor
 */
global.URLEncode = function(str)
{
  return urlencode.encode(str);
};