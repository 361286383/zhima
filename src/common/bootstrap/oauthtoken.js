/**
 * Created by peipeidediannao on 16/10/25.
 */
"use strict"

import schedule from 'node-schedule'
import log4js from 'log4js';

/**配置日志*/
log4js.configure(think.config('serverlog'));
const logger = log4js.getLogger('qianzhan-zmxy');

/**获取oauth token*/
let getOauthToken = () => {
    think.http("/zmxy/web/gettoken", true);
}

/**项目启动执行更新token方法*/
getOauthToken()

/**定时任务方法*/
let rule = new schedule.RecurrenceRule()
rule.hour = [0, 8, 16]
schedule.scheduleJob(rule, function () {
    logger.trace('====>执行更新oauth token定时任务<====')
    getOauthToken()
})