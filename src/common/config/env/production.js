'use strict';

export default {
    port: 20010, //将监听的端口修改为 1234
    serverlog:{		//log4js 服务器日志配置项
        appenders: [
            { type: 'console' },
            {
                type:'dateFile',
                filename: "/data/ProjectServer/node-zmxy-20010/logs/qianzhan-zmxy",
                pattern: "_yyyy-MM-dd.log",
                alwaysIncludePattern: true,
                category: "qianzhan-zmxy"
            }
        ],
        replaceConsole: true
    },
    db2:{//第二个数据库配置
        type: 'mysql',
        adapter: {
            mysql: {
                host: '192.168.148.106 ',
                port: '3312',
                database: 'openline',
                user: 'openline_dev',
                password: '8e62ec5fa7871c29',
                prefix: 'credit_app_',
                encoding: 'utf8'
            },
            mongo: {

            }
        }
    },
    url:{
        oauthUrl: `https://api.puhuifinance.com/uaa/oauth/token`
    },
    account:{
        clientId: `qianzhan-zmxy-nodejs`,
        secret: `jNRUQ7jyBQy6oyWz`
    }
};