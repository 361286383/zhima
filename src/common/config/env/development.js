'use strict';

export default {
    port: 8081, //将监听的端口修改为 1234
    serverlog:{		//log4js 服务器日志配置项
        appenders: [
            { type: 'console' },
            {
                type:'dateFile',
                filename: think.ROOT_PATH+"/logs/qianzhan-zmxy",
                pattern: "_yyyy-MM-dd.log",
                alwaysIncludePattern: true,
                category: "qianzhan-zmxy"
            }
        ],
        replaceConsole: true
    },
    
    db2:{//第二个数据库配置
        type: 'mysql',
        adapter: {
            mysql: {
                host: '10.10.229.152',
                port: '3306',
                database: 'openline',
                user: 'openline_app',
                password: 'openlinepuhui',
                prefix: 'credit_app_',
                encoding: 'utf8'
            },
            mongo: {

            }
        }
    },
    url:{
        oauthUrl: `http://t.uaa.pub.puhuifinance.com:8082/uaa/oauth/token`
    },
    account:{
        clientId: `qianzhan-zmxy-nodejs`,
        secret: `qianzhan-zmxy-nodejs`
    }
};