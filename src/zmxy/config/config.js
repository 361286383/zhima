/**
 * Created by peipeidediannao on 16/8/29.
 */
/**
 * 定义对接芝麻信用的常量
 */
export default{
    keystr:{
        ownerPublicDer:'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCW+mhxLvCgBp2Mq20jFb249k68GM+aqmW1Q7csFUTts3gE/igbbrHDk8moCtECx8B+d3NwaJUIjCpBCCcSKRerPPUBBlTSyb538rH0cHBkM5b5E9Dk8deiVoI3+Fosp4TsFNuBwXyeZcj4scyHHHSJV9HK5x1Dv6PDBbWa+WUbVwIDAQAB',
        ownerPrivateDer:`-----BEGIN RSA PRIVATE KEY-----
MIICWwIBAAKBgQCW+mhxLvCgBp2Mq20jFb249k68GM+aqmW1Q7csFUTts3gE/igb
brHDk8moCtECx8B+d3NwaJUIjCpBCCcSKRerPPUBBlTSyb538rH0cHBkM5b5E9Dk
8deiVoI3+Fosp4TsFNuBwXyeZcj4scyHHHSJV9HK5x1Dv6PDBbWa+WUbVwIDAQAB
AoGAOaOH1A4Tl0RvSZD4rXhTYhXGoVDnkSiUg8tCVvYXxpfJrj5JmY99FimJxzm5
6dHQz3dS/wsuOoEocINoRyqg+gvojCY8mgSPLUQ6qbsKZi/HFLNOTdzy7s7PyhSB
rbp3n6ZsEKwPX6SC+WH+VqhNRgU+p2svl3xSpMkun7gqVCECQQDatIz/EfhSsrNt
Ix2Z3NP45KBBl5MDlScx6hE4crhyh5kufuK7c0edzhMlKHCIG2tXzX+vbwc57ABS
1cEEipJ9AkEAsLlFGOSi37nFjQxiTQTBcUfZbl8f91ZfySLiuboAdpUE/o1/YrIA
1cllb+my5QQ11btlRPN/ucviqim3QzVZYwJABH+aNtTjyq+qSSBjx3pnUcNCGJdJ
itVSjRBHcmXJrXFkkTRC7aAdZP2MH0OJrQMDXjb33V/nQiW8i/ehlLpU8QJAP+1V
FHMog9u0nZg8OvYcb7j461cXviYNkjm/YEXrgvnXC6ntyarwaGVo/zm/j96dBsq6
EF4w9unM/ZcHhxVWVwJAOKZdoMhJV7Bm3ob6ACLNdvNgld8g31SlKseHxcHJDSyG
ayqY06WwzlL2p182yNKAirlJS4EO83jawHjp9gMCQg==
-----END RSA PRIVATE KEY-----`,
        zmxy_key:`-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCL2M4jZitbwhWqvCtWmh5XFBMg
SbSVz45KK0y5BnV7812ko/dEsipBFrclwGyrKUHq+6jn+aJ3a6gJND5q5+04mIeQ
YgG1vCDmMReE+ZvKoAztK0bu/5oxBMTco8hwVEFny5Z6q9P5pEh+2JZ9aJmRdIhL
fQEFHUNBEiPGUnoDhQIDAQAB
-----END PUBLIC KEY-----`,
        zmxy_api_url:'https://zmopenapi.zmxy.com.cn/openapi.do'
    },
    userInfo:{
        documentNo:'',
        name:'',
        userId:''
    },
    oauthToken: ''

}