/**
 * Created by peipeidediannao on 16/8/29.
 */

'use strict';
/**
 * 本文件只执行一次,生成公钥和私钥
 */

let NodeRSA = require('node-rsa');
let log4js = require('log4js');

let key = new NodeRSA({b:1024});
let publicDer = key.exportKey('pkcs8-public-der');
let privateDer = key.exportKey('pkcs1-der');

console.log(publicDer.toString('base64'));
console.log(privateDer.toString('base64'));

/*存储公钥和私钥*/
/*
 -----BEGIN PUBLIC KEY-----
 MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCW+mhxLvCgBp2Mq20jFb249k68GM+aqmW1Q7csFUTts3gE/igbbrHDk8moCtECx8B+d3NwaJUIjCpBCCcSKRerPPUBBlTSyb538rH0cHBkM5b5E9Dk8deiVoI3+Fosp4TsFNuBwXyeZcj4scyHHHSJV9HK5x1Dv6PDBbWa+WUbVwIDAQAB
 -----END PUBLIC KEY-----
 -----BEGIN RSA PRIVATE KEY-----
 MIICWwIBAAKBgQCW+mhxLvCgBp2Mq20jFb249k68GM+aqmW1Q7csFUTts3gE/igbbrHDk8moCtECx8B+d3NwaJUIjCpBCCcSKRerPPUBBlTSyb538rH0cHBkM5b5E9Dk8deiVoI3+Fosp4TsFNuBwXyeZcj4scyHHHSJV9HK5x1Dv6PDBbWa+WUbVwIDAQABAoGAOaOH1A4Tl0RvSZD4rXhTYhXGoVDnkSiUg8tCVvYXxpfJrj5JmY99FimJxzm56dHQz3dS/wsuOoEocINoRyqg+gvojCY8mgSPLUQ6qbsKZi/HFLNOTdzy7s7PyhSBrbp3n6ZsEKwPX6SC+WH+VqhNRgU+p2svl3xSpMkun7gqVCECQQDatIz/EfhSsrNtIx2Z3NP45KBBl5MDlScx6hE4crhyh5kufuK7c0edzhMlKHCIG2tXzX+vbwc57ABS1cEEipJ9AkEAsLlFGOSi37nFjQxiTQTBcUfZbl8f91ZfySLiuboAdpUE/o1/YrIA1cllb+my5QQ11btlRPN/ucviqim3QzVZYwJABH+aNtTjyq+qSSBjx3pnUcNCGJdJitVSjRBHcmXJrXFkkTRC7aAdZP2MH0OJrQMDXjb33V/nQiW8i/ehlLpU8QJAP+1VFHMog9u0nZg8OvYcb7j461cXviYNkjm/YEXrgvnXC6ntyarwaGVo/zm/j96dBsq6EF4w9unM/ZcHhxVWVwJAOKZdoMhJV7Bm3ob6ACLNdvNgld8g31SlKseHxcHJDSyGayqY06WwzlL2p182yNKAirlJS4EO83jawHjp9gMCQg==
 -----END RSA PRIVATE KEY-----



 测试芝麻公钥
 MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCL2M4jZitbwhWqvCtWmh5XFBMgSbSVz45KK0y5BnV7812ko/dEsipBFrclwGyrKUHq+6jn+aJ3a6gJND5q5+04mIeQYgG1vCDmMReE+ZvKoAztK0bu/5oxBMTco8hwVEFny5Z6q9P5pEh+2JZ9aJmRdIhLfQEFHUNBEiPGUnoDhQIDAQAB
 
 生产芝麻公钥
 MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC8VC601lNMFQWTHwGRpetqbZsNzPsBehfSeJED6fV58eTmHRp+xdO63C4xb23oda/ePGwVuu0P//E8uZ0I/SeqX6aHRtsQj8r8paKgblFBFxgXnYgaeI75SgSZR5+TmUTPO3RqlKdj1MDd2OKfhVZyeih7VBGjZ7+/nh7GY8Z0IQIDAQAB

*/










