/**
 * Created by peipeidediannao on 16/8/29.
 */
'use strict';

import Base from './base.js';
import NodeRSA from 'node-rsa';
import log4js from 'log4js';
import urlEncode from 'urlencode';
import queryString from 'querystring';
import rp from 'request-promise';


//配置日志
log4js.configure(think.config('serverlog'));
const logger = log4js.getLogger('qianzhan-zmxy');


export default class extends Base  {
    /**
     *获取oauth token方法
     */
    async gettokenAction(){

        logger.trace('====>开始获取oauthToken<====')
        let clientId = `${think.config('account.clientId')}`
        let secret = `${think.config('account.secret')}`
        let param_account = this.getBasicAuthorization(clientId,secret)
        logger.info('生成帐号参数param_account==>'+param_account)

        let oarthUrl = `${think.config('url.oauthUrl')}`
        let requestUrl = `${oarthUrl}?grant_type=client_credentials`
        logger.info('得到请求oauth服务器地址参数==>'+requestUrl)

        let res = await rp({
            method: 'POST',
            url: requestUrl,
            headers: {
                'content-type': 'application/x-www-form-urlencoded',
                'Authorization': param_account
            },
            json: true,
            gzip: true
        });
        logger.info('得到请求oauth服务返回结果==>'+JSON.stringify(res))
        let token_type = res.token_type
        let  token_type_new = token_type.charAt(0).toUpperCase() + token_type.slice(1)
        let auth = `${token_type_new} ${res.access_token}`
        this.config('oauthToken',auth)
    }
    /**
     *访问芝麻信用授权接口方法
     */
    async indexAction(){

        logger.trace('==>进入/zmxy/web/index方法');
        logger.trace('==>首先清理config缓存');

        this.config('userInfo.documentNo','')
        this.config('userInfo.name','')
        this.config('userInfo.userId','')

        let _this = this.http;

        if(!_this.isGet()){
            logger.fatal('==>请求方式错误')
            let errorUrl =`${this.errMsg('请求方式错误')}`
            logger.trace('==>错误跳转URL'+errorUrl)
            return _this.redirect(errorUrl);
        }
        logger.info('==>请求方式为get')

        //logger.trace('==>开始访问后台方法传入用户credit_app_customer_id');
        logger.trace('==>获取传入参数userId');
        let userId = _this.param('userId')
        if(userId == ''){
            logger.fatal('==>userId为空')
            let errorUrl =`${this.errMsg('传入参数为空')}`
            logger.trace('==>错误跳转URL'+errorUrl)
            return _this.redirect(errorUrl);
        }
        logger.info('==>传入参数userId==>'+userId)

        //重新定义数据库配置
        logger.trace('==>获取数据库配置参数');
        let db2_config = think.config('db2');
        logger.info('==>数据库配置信息'+db2_config)

        logger.trace('==>获取用户身份证和名字对应的表credit_app_customer');
        let customer_data = await think.model("customer", db2_config, "openline");
        let idno_name = await customer_data.where({id:userId}).find();
        if(think.isEmpty(idno_name)){
            logger.fatal('==>查询不到此用户')
            let errorUrl =`${this.errMsg('查询不到此用户')}`
            logger.trace('==>错误跳转URL'+errorUrl)
            return _this.redirect(errorUrl);
        }
        logger.info('==>获取用户身份证和名字对应的表credit_app_customer成功');
        let id_no = idno_name.id_no;
        let real_name = idno_name.real_name;
        if(id_no == ''){
            logger.fatal('==>获取用户身份证id_no失败')
            let errorUrl =`${this.errMsg('获取用户身份证失败')}`
            logger.trace('==>错误跳转URL'+errorUrl)
            return _this.redirect(errorUrl);
        }
        logger.info('==>获取用户身份证成功id_no==>'+id_no);

        if(real_name == ''){
            logger.fatal('==>获取用户姓名real_name失败')
            let errorUrl =`${this.errMsg('获取用户姓名失败')}`
            logger.trace('==>错误跳转URL'+errorUrl)
            return _this.redirect(errorUrl);
        }
        logger.info('==>获取用户姓名成功real_name==>'+real_name);

        //定义透传参数,为了让芝麻信用返回能够匹配到用户
        let biz_params = {
            "auth_code":"M_H5",//用来传递授权模式的,使用证件号姓名为入参授权的模式auth_code=M_H5;
            "state":''//用于给商户提供透传的参数，芝麻会将此参数透传给商户;
        }

        logger.trace('==>暂时存储id_no  real_name userId');
        let id_state = `${id_no}&${real_name}&${userId}`;
        biz_params.state = id_state;
        logger.trace('==>存储用户透传参数'+id_state);
        this.config('userInfo.documentNo',id_no)
        this.config('userInfo.name',real_name)
        this.config('userInfo.userId',userId)


        logger.trace('==>开始访问大数据查询用户openid是否存在');
        let url = this.calldataUrl();
        logger.info('根据环境获取大数据服务地址成功url==>'+url);
        let dataAccount = this.dataAccount();
        logger.info('根据环境获取访问大数据接口的帐号成功dataAccount==>'+dataAccount);
        logger.trace('==>访问大数据接口/credit/zmxy/checkOpenId');
        let checkOpenId_urlparam = `${url}/credit/zmxy/checkOpenId?documentNo=${id_no}&name=${urlEncode.encode(real_name)}${dataAccount}`
        logger.trace('/credit/zmxy/checkOpenId接口URL参数==>'+checkOpenId_urlparam)
        let judge_openid_exist = await rp({
            method: 'GET',
            url:checkOpenId_urlparam,
            headers:{
                'content-type': 'application/json',
                'Authorization': this.config('oauthToken')
            },
            json: true,
            gzip: true
        });
        logger.trace('大数据接口/credit/zmxy/checkOpenId返回结果==>'+JSON.stringify(judge_openid_exist))

        logger.trace('==>获取数据库芝麻信用对应的表credit_app_zmxy')
        let zmxy_data = await think.model("zmxy", db2_config, "openline");

        let annex_status = await think.model("annex_status", db2_config, "openline");
        logger.trace('==>获取用户数据');
        let customerData=await annex_status.where({credit_app_customer_id: userId});
        logger.info('==>获取用户数据'+customerData);

        let hrefApp_url = this.hrefappUrl();
        logger.info('==>获取跳转回app落地页面的成功URL成功'+hrefApp_url);
        if(judge_openid_exist.success == true){
            let zmxy_type =await customerData.where({type:10});
            await zmxy_type.update({
                status:3,
                submit_time:think.datetime()
            });
            logger.info('==>更新芝麻信用认证状态为3');
            await zmxy_data.where({credit_app_customer_id: userId}).update({zmxy_open_id:judge_openid_exist.openId})
            logger.info('更新本地库credit_app_zmxy用户的openid成功');
            logger.info('==>跳转回app落地页面');
            let errorUrl =`${this.errMsg('您已进行过芝麻信用授权')}`
            logger.trace('==>跳转回app落地页面URL'+errorUrl)
            return _this.redirect(errorUrl);

        }else if(judge_openid_exist.success == false){
            //芝麻公钥导入方式
            const zm_crt_option = {
                encryptionScheme: 'pkcs1'
            }
            //自己私钥导入方式
            const my_key_option = {
                signingScheme: 'pkcs1-sha1'
            }
            //读取芝麻公钥和自己私钥
            let read_owner_private_key = this.config('keystr.ownerPrivateDer');
            //let read_zhima_public_key = this.config('keystr.zmxy_key');
            let read_zhima_public_key = this.getzmKey();

            logger.info('==>商户私钥'+read_owner_private_key);
            logger.info('==>芝麻公钥'+read_zhima_public_key);


            let owner_private_key = new NodeRSA(read_owner_private_key, my_key_option);
            let zhima_public_key = new NodeRSA(read_zhima_public_key,zm_crt_option);


            let encryptParam = function (paramsPlain,padding='RSA_NO_PADDING') {
                return urlEncode.encode(zhima_public_key.encrypt(paramsPlain,'base64'));
            }
            let encryptSign = function (paramsPlain) {
                return urlEncode.encode(owner_private_key.sign(paramsPlain).toString('base64'));
            }

            let toUrlParam = function (args) {
                let keys = Object.keys(args);
                keys = keys.sort()
                let newArgs = {};
                keys.forEach(function (key) {
                    newArgs[key.toLowerCase()] = args[key];
                });

                let string = '';
                for (let k in newArgs) {
                    string += '&' + k + '=' + newArgs[k];
                }
                string = string.substr(1);
                return string;
            }


            let baseurl = this.config('keystr.zmxy_api_url')

            let getFullApiUrl = function (sysParam,bizParam) {
                //let baseurl = this.config('keystr.zmxy_api_url')
                let params_encrypted = encryptParam(bizParam)
                let sign_encrypted = encryptSign(bizParam)
                let FullApiUrl = `${baseurl}?${toUrlParam(sysParam)}&params=${params_encrypted}&sign=${sign_encrypted}`
                return FullApiUrl
            }

            //系统参数
            let app_id = this.getappId();
            let sys_param = {
                app_id: app_id,
                charset: 'UTF-8',
                method: 'zhima.auth.info.authorize',
                scene: 'testQianzhan',
                version: '1.0',
                channel: 'app',
                platform: 'zmop'
            }

            //业务参数
            let identity_type = 2;//按照身份证+姓名进行授权
            let identity_param = {
                certNo: "",
                name: "",
                certType: "IDENTITY_CARD"
            }
            /*identity_param.certNo=this.config('userInfo.documentNo');
            identity_param.name=this.config('userInfo.name');*/
            identity_param.certNo=this.config('userInfo.documentNo');
            identity_param.name=this.config('userInfo.name');

            logger.trace('==>从缓存中读取certNo和name'+identity_param.certNo+' '+identity_param.name)

            logger.trace('==>请求芝麻信用之前的业务参数'+JSON.stringify(identity_param))
            /*let biz_params = {
                "auth_code":"M_H5",//用来传递授权模式的,使用证件号姓名为入参授权的模式auth_code=M_H5;
                "state":"20160901"//用于给商户提供透传的参数，芝麻会将此参数透传给商户;
            }*/
            let biz_params_all = 'identity_type=' + identity_type + '&identity_param=' + urlEncode(JSON.stringify(identity_param))+'&biz_params='+urlEncode(JSON.stringify(biz_params))

            logger.trace('==>传给芝麻信用加密之前的参数'+biz_params_all);


            let request_url = getFullApiUrl(sys_param, biz_params_all)
            logger.info('==>成功获取请求芝麻信用的URL'+request_url)
            return _this.redirect(request_url);
        }

    }

    async zmxycallbackAction(){

        logger.trace('==>请求借口')
        let url=this.http.url;
        logger.info('==>获取芝麻信用返回的URL成功'+url);
        let call_back_data=this.http.query;
        /**
         * jie解密芝麻返回业务参数
         * @param paramsPlain
         * @returns {Buffer|Object|string}
         */
        const my_key_option = {
            signingScheme: 'pkcs1-sha1'
        }
        //读取芝麻公钥和自己私钥
        let read_owner_private_key = this.config('keystr.ownerPrivateDer');
        logger.info('==>私钥'+read_owner_private_key);
        function decryptParam (paramsPlain) {
            let owner_private_key = new NodeRSA(read_owner_private_key, my_key_option);
            owner_private_key.setOptions({encryptionScheme: 'pkcs1'});
            return owner_private_key.decrypt(paramsPlain,'utf8')
        }

        let call_params = urlEncode.decode(call_back_data.params,'utf8')
        let call_params_end=queryString.parse(decryptParam(call_params));
        logger.info('==>解析芝麻信用返回结果成功'+JSON.stringify(call_params_end));
        

        let get_state = call_params_end.state;
        let stateArr = get_state.split('&');
        logger.info('==>获取芝麻信用透传参数成功[id_no,real_name,userId]'+stateArr)

        if(call_params_end.success == 'true'){
            logger.trace('==>访问大数据接口/credit/zmxy/saveOpenId');
            let url = this.calldataUrl();
            logger.info('根据环境获取大数据服务地址成功url==>'+url);
            let dataAccount = this.dataAccount();
            logger.info('根据环境获取访问大数据接口的帐号成功dataAccount==>'+dataAccount);
            /*let id_no = this.config('userInfo.documentNo')
            let real_name = this.config('userInfo.name')*/
            let id_no = stateArr[0];
            let real_name = stateArr[1];
            logger.info('==>从缓存中读取id_no real_name'+id_no+''+real_name);
            let open_id = call_params_end.open_id;
            logger.info('==>成功获取open_id'+open_id)



            let checkOpenId_urlparam = `${url}/credit/zmxy/saveOpenId?documentNo=${id_no}&name=${urlEncode.encode(real_name)}${dataAccount}&openId=${open_id}`
            logger.trace('/credit/zmxy/saveOpenId接口URL参数==>'+checkOpenId_urlparam)
            let judge_openid_exist = await rp({
                method: 'GET',
                url:checkOpenId_urlparam,
                headers:{
                    'content-type': 'application/json',
                    'Authorization': this.config('oauthToken')
                },
                json: true,
                gzip: true
            });
            logger.trace('==>获取大数据返回结果'+JSON.stringify(judge_openid_exist))

            //let userId = this.config('userInfo.userId')
            let userId = stateArr[2];
            if(userId == ''){
                logger.fatal('==>从缓存中读取userId失败')
            }else{
                logger.info('==>从缓存中读取userId成功'+userId)
            }

            //重新定义数据库配置
            logger.trace('==>获取数据库配置参数');
            let db2_config=think.config('db2');

            logger.trace('==>获取用户数据');
            let annex_status = await think.model("annex_status", db2_config, "openline");
            let customerData=await annex_status.where({credit_app_customer_id: userId});
            let zmxy_status = await customerData.where({type:10});
            if(think.isEmpty(zmxy_status)){
                logger.fatal('==>查询不到此用户芝麻信用授信状态')
                let errorUrl =`${this.errMsg('查询不到此用户芝麻信用授信状态')}`
                logger.trace('==>查询不到此用户芝麻信用授信状态'+errorUrl)
                return _this.redirect(errorUrl);
            }
            if(judge_openid_exist.success == true){
                await zmxy_status.update({
                    status:3,
                    submit_time:think.datetime()
                });
                logger.info('==>更新芝麻信用认证状态为3');


                logger.trace('==>获取数据库芝麻信用对应的表credit_app_zmxy')
                let zmxy_data = await think.model("zmxy", db2_config, "openline");
                //await zmxy_data.where({credit_app_customer_id: userId}).update({zmxy_open_id:judge_openid_exist.openId})
                await zmxy_data.thenAdd({
                    credit_app_customer_id: userId,
                    zmxy_open_id:judge_openid_exist.openId,
                    create_time:think.datetime(),
                    update_time:think.datetime()
                }, {
                    credit_app_customer_id: userId
                });
                logger.info('更新本地库credit_app_zmxy用户的openid成功');
                logger.info('==>跳转回app落地页面');
                let hrefApp_url = this.hrefappUrl();
                return this.redirect(hrefApp_url);

            }else if(judge_openid_exist.success == false){
                await zmxy_status.update({
                    status:4,
                    submit_time:think.datetime()
                });
                logger.fatal('==>更新芝麻信用认证状态失败==>4');
                let errorUrl =`${this.errMsg('抱歉，由于网络原因，暂时无法获取您的芝麻信用信息，您可以稍后再进行尝试')}`
                logger.trace('==>存储芝麻信用openID失败:'+errorUrl)
                return this.redirect(errorUrl);
            }
        }else if(call_params_end.success == 'false'){
            logger.fatal('==>用户授权芝麻信用失败:'+call_params_end.error_message);
            logger.trace('==>跳转回app落地页面');
            let hrefApp_url = this.hrefappUrl();
            return this.redirect(hrefApp_url);
        }

    }

    /**
     *对用户名和密码进行处理
     * @param username
     * @param password
     * @returns {string}
     */
    getBasicAuthorization(username,password){
        return 'Basic ' + new Buffer(username + ':' + password).toString('base64')
    }













}

