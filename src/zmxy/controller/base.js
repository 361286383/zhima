'use strict';

import urlEncode from 'urlencode';

export default class extends think.controller.base {
    /**
     * 根据环境返回大数据接口地址
     */
    calldataUrl () {
        let url = '';
        switch (think.env) {
            case 'development':
                url = 'http://ut1.zuul.pub.puhuifinance.com:8765/credit-query/api';
                break;
            case 'testing':
                url = 'http://ut1.zuul.pub.puhuifinance.com:8765/credit-query/api';
                break;
            case 'production':
                url = 'https://api.puhuifinance.com/credit-query/api';
                break;
            default:
                break;
        }
        return url;
    }

    /**
     * 调用大数据接口需要账户密码,根据环境
     * @returns {string}
     */
    dataAccount (){
        let urlParam = '';
        switch (think.env) {
            case 'development':
                urlParam = '&username=lend&password=vQHntIOHBIQc';
                break;
            case 'testing':
                urlParam = '&username=lend&password=vQHntIOHBIQc';
                break;
            case 'production':
                urlParam = '&username=qzzy&password=cec86aad293557e4123d4f31103c2549';
                break;
            default:
                break;
        }
        return urlParam;
    }

    /**
     * 根据运行环境返回在对接芝麻信用后跳转回app的成功URL
     * @returns {string}
     */
    hrefappUrl(){
        let urlHref = '';
        switch (think.env) {
            case 'development':
                urlHref = 'http://123.59.80.37/interacts/success.html';
                break;
            case 'testing':
                urlHref = 'http://123.59.80.37/interacts/success.html';
                break;
            case 'production':
                urlHref = 'http://app-h5.iqianzhan.com/interacts/success.html';
                break;
            default:
                break;
        }
        return urlHref;
    }

    /**
     * 根据环境返回测试还是生产的芝麻公钥
      * @returns {string|string}
     */
    getzmKey(){
        let key = ``;
        switch (think.env) {
            case 'development':
                key = `-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCL2M4jZitbwhWqvCtWmh5XFBMg
SbSVz45KK0y5BnV7812ko/dEsipBFrclwGyrKUHq+6jn+aJ3a6gJND5q5+04mIeQ
YgG1vCDmMReE+ZvKoAztK0bu/5oxBMTco8hwVEFny5Z6q9P5pEh+2JZ9aJmRdIhL
fQEFHUNBEiPGUnoDhQIDAQAB
-----END PUBLIC KEY-----`;
                break;
            case 'testing':
                key = `-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCL2M4jZitbwhWqvCtWmh5XFBMg
SbSVz45KK0y5BnV7812ko/dEsipBFrclwGyrKUHq+6jn+aJ3a6gJND5q5+04mIeQ
YgG1vCDmMReE+ZvKoAztK0bu/5oxBMTco8hwVEFny5Z6q9P5pEh+2JZ9aJmRdIhL
fQEFHUNBEiPGUnoDhQIDAQAB
-----END PUBLIC KEY-----`;
                break;
            case 'production':
                key = `-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC8VC601lNMFQWTHwGRpetqbZsN
zPsBehfSeJED6fV58eTmHRp+xdO63C4xb23oda/ePGwVuu0P//E8uZ0I/SeqX6aH
RtsQj8r8paKgblFBFxgXnYgaeI75SgSZR5+TmUTPO3RqlKdj1MDd2OKfhVZyeih7
VBGjZ7+/nh7GY8Z0IQIDAQAB
-----END PUBLIC KEY-----`;
                break;
            default:
                break;
        }
        return key;
    }

    /**
     * 根据服务运行环境判断对接芝麻信用的商户app_id
     * @returns {string}
     */
    getappId(){
        let app_id = '';
        switch (think.env) {
            case 'development':
                app_id = '1000718';
                break;
            case 'testing':
                app_id = '1000718';
                break;
            case 'production':
                app_id = '1000762';
                break;
            default:
                break;
        }
        return app_id;
    }

    /**
     * 当程序运行出错时返回给前端的错误信息
     * @param text
     * @returns {string}
     */
    errMsg(text){
        let returnUrl = ``;
        let tex = urlEncode.encode(urlEncode.encode(text));
        switch (think.env) {
            case 'development':
                returnUrl = `http://123.59.80.37/dataCrawling/creditZhima.html?error=${tex}`;
                break;
            case 'testing':
                returnUrl = `http://123.59.80.37/dataCrawling/creditZhima.html?error=${tex}`;
                break;
            case 'production':
                returnUrl = `http://app-h5.iqianzhan.com/dataCrawling/creditZhima.html?error=${tex}`;
                break;
            default:
                break;
        }
        return returnUrl;
    }


}